#!/usr/bin/env python

import nltk
import random

def init_db():
    nltk.download("wordnet")
    nltk.download("omw-1.4")


class Wordle:

    def __init__(self, word_len=5):
        self.all_words = []
        self.word_len = word_len
        self.guesses = []
        self.unused_letters = []
        self.used_letters = []
        for word in nltk.corpus.wordnet.words():
            if len(word) == self.word_len:
                self.all_words.append(word)

    def check_unused_letters(self, word):
        for letter in self.unused_letters:
            if letter in word:
                return False
        return True

    def check_used_letters(self, word):
        for bit, letter in self.used_letters:
            if bit == None:
                if letter not in word:
                    return False
            elif letter != word[bit]:
                return False
        return True

    def update_words_list(self):
        updated_words = []
        for word in self.all_words:
            if self.check_unused_letters(word) == False:
                continue
            if self.check_used_letters(word) == False:
                continue
            updated_words.append(word)
        print("Words list went from {} to {}".format(len(self.all_words), len(updated_words)))
        self.all_words = updated_words

    def add_guess(self, guess, word_bits):
        guess = guess.lower()
        if len(guess) != self.word_len:
            print("Error! Guess must be {}, it was {}".format(self.word_len, len(guess)))
        if len(word_bits) != self.word_len:
            print("Error! Bits must be {}, it was {}".format(self.word_bits, len(guess)))
        for i, bit in enumerate(word_bits):
            if bit == 0:
                self.unused_letters.append(guess[i])
            if bit == 1:
                self.used_letters.append([None, guess[i]])
            if bit == 2:
                self.used_letters.append([i, guess[i]])
        print(self.used_letters)
        self.update_words_list()
    
    def suggest(self):
        print(random.choice(self.all_words))

my_wordle = Wordle()
my_wordle.add_guess("halts", [0, 0, 0, 0, 0])
my_wordle.add_guess("brine", [0, 0, 0, 2, 0])
my_wordle.add_guess("moony", [0, 2, 1, 2, 0])
my_wordle.add_guess("wound", [0, 2, 2, 2, 2])
my_wordle.suggest()
